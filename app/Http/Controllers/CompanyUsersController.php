<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyUsers;
use Illuminate\Http\Request;

class CompanyUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CompanyUsers::where('company_id',$request->id)->delete();

        $data = [];
        foreach ($request->users as $userId){
            $data[]=['company_id'=>$request->id,'user_id'=>$userId];
        }

        $company = CompanyUsers::insert($data);
        return response()->json([
            'message'=>'Company Users Created Successfully!!',
            'company'=>$company
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CompanyUsers  $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyUsers $companyUsers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CompanyUsers  $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyUsers $companyUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CompanyUsers  $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyUsers $companyUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CompanyUsers  $companyUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyUsers $companyUsers)
    {
        //
    }
}
