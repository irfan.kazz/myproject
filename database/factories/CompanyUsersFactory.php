<?php

namespace Database\Factories;

use App\Models\CompanyUsers;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyUsersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompanyUsers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
