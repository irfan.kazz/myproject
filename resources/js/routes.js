const Welcome = () => import('./components/Welcome.vue' /* webpackChunkName: "resource/js/components/welcome" */)
const UserList = () => import('./components/user/List.vue' /* webpackChunkName: "resource/js/components/user/list" */)
const UserCreate = () => import('./components/user/Add.vue' /* webpackChunkName: "resource/js/components/user/add" */)
const UserEdit = () => import('./components/user/Edit.vue' /* webpackChunkName: "resource/js/components/user/edit" */)
const CompanyList = () => import('./components/company/List.vue' /* webpackChunkName: "resource/js/components/company/list" */)
const CompanyCreate = () => import('./components/company/Add.vue' /* webpackChunkName: "resource/js/components/company/add" */)
const CompanyEdit = () => import('./components/company/Edit.vue' /* webpackChunkName: "resource/js/components/company/edit" */)
const CompanyUserEdit = () => import('./components/companyUser/Add.vue' /* webpackChunkName: "resource/js/components/companyUser/add" */)

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Welcome
    },
    {
        name: 'userList',
        path: '/user',
        component: UserList
    },
    {
        name: 'userEdit',
        path: '/user/:id/edit',
        component: UserEdit
    },
    {
        name: 'userAdd',
        path: '/user/add',
        component: UserCreate
    },
    {
        name: 'companyList',
        path: '/company',
        component: CompanyList
    },
    {
        name: 'companyEdit',
        path: '/company/:id/edit',
        component: CompanyEdit
    },
    {
        name: 'companyAdd',
        path: '/company/add',
        component: CompanyCreate
    },
    {
        name: 'CompanyUserEdit',
        path: '/companyUsers/:id/edit',
        component: CompanyUserEdit
    },
]
